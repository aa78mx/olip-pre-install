# Ideascube Cleanup & Configure

This playbook cleanup the CMAL device from previous software and configure it.


## Initialization

```
curl -sfL https://gitlab.com/bibliosansfrontieres/olip-bsf/olip-pre-install/-/raw/master/go.sh | bash -s -- --name idc-fra-name
```